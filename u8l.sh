#!/bin/bash

rm -rf vendor/ {internal,middleware,utils}/ .github/
find . -name "*.go" | grep -vE '(input|output|io_test)\.go' | xargs rm -f
cp -r ../fiber/{internal,log,middleware,utils} ./
cp -r ../fiber/.github ./
cp ../fiber/*.go ./
find . -name "*.go" | xargs chmod -x
find . -name "*.go" | xargs sed -i 's/github\.com\/gofiber\/fiber/gitee\.com\/azhai\/fiber-u8l/g'
sed -i 's/cBlack = /\/\/ cBlack = /' app.go
sed -i 's/\/\/ cYellow/cYellow/' app.go
sed -i '/^\tvalue :=/i\\tvar cGround = cYellow' app.go
sed -i 's/cBlack/cGround/g' app.go
sed -i '/\tif mainLe/i\\tblankLine := strings.Repeat(" ", 54)' app.go
sed -i 's/Logo\, ""/Logo\, blankLine/' app.go

ver="${1##*(v)}"
sed -i "/const Version/cconst Version = \"$ver\"" app.go
