module gitee.com/azhai/fiber-u8l/v2

go 1.20

require (
	github.com/google/uuid v1.3.0
	github.com/mattn/go-colorable v0.1.13
	github.com/mattn/go-isatty v0.0.19
	github.com/mattn/go-runewidth v0.0.14
	github.com/stretchr/testify v1.8.3
	github.com/tinylib/msgp v1.1.8
	github.com/valyala/bytebufferpool v1.0.0
	github.com/valyala/fasthttp v1.47.0
	golang.org/x/sys v0.8.0
)

require (
	github.com/andybalholm/brotli v1.0.5 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/klauspost/compress v1.16.5 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
